# Initialize new server
* create digitalocean server
* add <ip> to package.json
* ./server-env/scripts/init-server.sh <ip> <port>
* gulp release -s


# Backup MongoDB job
* ssh-copy-id -i ~/.ssh/id_rsa.pub lucy@
* 59 04 * * * ~/env/backup-mongodb-job.sh lucy v6.4 lucy@ ipspool >> ~/backup-mongodb-job.log 2>&1


# crontab
* crontab -e
* sudo grep CRON /var/log/syslog
