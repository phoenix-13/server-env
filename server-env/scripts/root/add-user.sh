USER=$1
PASSWORD=$2


adduser --disabled-password --gecos "" $USER
echo "$USER:$PASSWORD" | chpasswd
echo "$USER ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
echo "$USER ALL=NOPASSWD: /usr/sbin/service, /bin/chown" >> /etc/sudoers

printf "\n\n__LUCY__: Added user ($USER)\n\n\n"
